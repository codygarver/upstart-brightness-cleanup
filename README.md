Upstart save and restore brightness scripts
===========================================

Copyright 2015 Jorge Juan <jjchico@gmail.com>

Licence
-------

These files are free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

If you need other licencing conditions please contact the author.

Description
-----------

Save screen devices brightness at shutdown and restores it at bootup.

Tested on: Ubuntu 14.04

Should work on any Upstart-enabled system with a screen device which
brightness can be controlled through /sys/class/backlight/*/brightness
devices.

Installation
------------

  1. Download both *.store files.
  2. Copy the files to /etc/init (probably as root)

Example
-------

Open a terminal and execute these commands:

    cd /etc/init
    sudo wget https://bitbucket.org/jjchico/upstart-brightness/raw/d9a5d9484475eb1256d56c64b28e680a7aeae72c/brightness-store.conf
    sudo wget https://bitbucket.org/jjchico/upstart-brightness/raw/d9a5d9484475eb1256d56c64b28e680a7aeae72c/brightness-restore.conf

Set the desired brightness and restart your computer.

Troubleshooting
---------------

If your brightness is not saved/restored, open a terminal and execute:

    ls /sys/class/backlight/*/brightness

If you get an error saying no file was found, there is no screen which
brightness these scripts know how to control. If you solve it you may send
patches.

In my system I get:

    ls /sys/class/backlight/*/brightness
    /sys/class/backlight/intel_backlight/brightness
